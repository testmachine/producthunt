//
//  PostViewController.swift
//  ProductHunt
//
//  Created by MacBookPro on 12/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit
import WebKit

class PostViewController: UIViewController {
    
    var post: Post?
    var webView: WKWebView!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var screenshotImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    @IBAction func getButtonTapped(_ sender: UIButton) {
        if let post = post {
            webView = WKWebView(frame: UIScreen.main.bounds)
            view.addSubview(webView)
            if let url = URL(string: post.appStoreURL) {
                let request = URLRequest(url: url)
                webView.load(request)
            }
            
        }
    }
    
    func updateUI() {
        if let post = post {
            nameLabel.text = post.name.trimmingCharacters(in: .whitespacesAndNewlines)
            descriptionLabel.text = post.tagline.trimmingCharacters(in: .whitespacesAndNewlines)
            screenshotImageView.imageFromServerURL(urlString: post.screenshotURL)
            votesLabel.text = "\(post.votesCount)"
        }
    }

}
