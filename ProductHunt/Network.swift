//
//  Network.swift
//  ProductHunt
//
//  Created by MacBookPro on 11/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import Foundation
import Alamofire

class Network {
    
    let headers = [
        "Authorization":"Bearer 591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff"
    ]
    
    var postList: [Post] = []
    var tagList: [Tag] = []
    
    func getPosts(category: String, completionHandler: @escaping ([Post]?, Error?) -> ()) {
        makePostsCall(category: category, completionHandler: completionHandler)
    }
    
    func getTags(completionHandler:  @escaping ([Tag]?, Error?) -> ()) {
        makeTagsCall(completionHandler: completionHandler)
    }
    
    func makeTagsCall(completionHandler: @escaping ([Tag]?, Error?) -> ()) {
        Alamofire.request("https://api.producthunt.com/v1/topics", headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String:AnyObject] {
                    if let topics = json["topics"] as? [AnyObject] {
                        for item in topics {
                            if let topicJSON = item as? [String:AnyObject],
                                let tag = Tag(json: topicJSON){
                                self.tagList.append(tag)
                            }
                        }
                        completionHandler(self.tagList, nil)
                    }
                }
                
            case .failure(let error):
                completionHandler(nil, error)
                
            }
        }
    }
    
    func makePostsCall(category: String, completionHandler: @escaping ([Post]?, Error?) -> ()) {
        postList = []
        let url = "https://api.producthunt.com/v1/categories/\(category)/posts"
        Alamofire.request(url, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String:AnyObject] {
                    if let posts = json["posts"] as? [AnyObject] {
                        for item in posts {
                            if let postJSON = item as? [String:AnyObject],
                                let post = Post(json: postJSON){
                                self.postList.append(post)
                            }
                        }
                        completionHandler(self.postList, nil)
                    }
                }
            
            case .failure(let error):
                completionHandler(nil, error)

            }
        }
        
    }
}
