//
//  PostViewCell.swift
//  ProductHunt
//
//  Created by MacBookPro on 12/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class PostViewCell: UITableViewCell {


    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var upvotes: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func fillCell(post: Post) {
        name.text = post.name.trimmingCharacters(in: .whitespacesAndNewlines)
        upvotes.text = "\(post.votesCount)"
        productDescription.text = post.tagline.trimmingCharacters(in: .whitespacesAndNewlines)
        productImage.imageFromServerURL(urlString: post.thumbnailURL)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImage.image = nil
    }

}
