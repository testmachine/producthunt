//
//  TagsViewController.swift
//  ProductHunt
//
//  Created by MacBookPro on 13/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class TagsViewController: UITableViewController {

    var tags: [Tag]?
    var selectedTag: Tag?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tags?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TagCell", for: indexPath)

        let label = cell.viewWithTag(1) as! UILabel
        if let tags = tags {
            label.text = tags[indexPath.row].name
        }
        return cell
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TagSelect" {
            if let indexPath = tableView.indexPath(
                for: sender as! UITableViewCell) {
                if let tags = tags {
                    let tag = tags[indexPath.row]
                    selectedTag = tag
                }
            }
            
        }
    }
}
