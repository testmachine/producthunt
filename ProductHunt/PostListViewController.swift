//
//  PostsViewController.swift
//  ProductHunt
//
//  Created by MacBookPro on 12/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class PostListViewController: UITableViewController {

    var posts: [Post]?
    var tags: [Tag]?
    var tag: Tag!
    var navBarButton = UIButton(type: .custom)
    let api = Network()
    var indicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBarButton =  UIButton(type: .custom)
        navBarButton.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 22)
        navBarButton.backgroundColor = UIColor(red:0.85, green:0.33, blue:0.18, alpha:1.0)
        navBarButton.setTitle("Tech", for: .normal)
        navBarButton.addTarget(self, action: #selector(self.clickOnButton), for: .touchUpInside)
        navBarButton.isEnabled = false
        
        self.navigationItem.titleView = navBarButton
        
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
        
        indicator.startAnimating()
        api.getPosts(category: "tech") {[weak self] response, error in
            self?.posts = response
            self?.indicator.stopAnimating()
            self?.tableView.reloadData()
        }
        
        api.getTags() {[weak self] response, error in
            self?.tags = response
            self?.navBarButton.isEnabled = true
        }
        
        refreshControl = UIRefreshControl()
        refreshControl!.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl!.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    @IBAction func unwindToPostList(segue: UIStoryboardSegue) {
        if let sourceViewController = segue.source as? TagsViewController {
            tag = sourceViewController.selectedTag
            if let tag = tag {
                navBarButton.setTitle(tag.name, for: .normal)
                posts?.removeAll()
                tableView.reloadData()
                indicator.startAnimating()
                api.getPosts(category: tag.slug) {[weak self] response, error in
                    self?.posts = response
                    self?.indicator.stopAnimating()
                    self?.tableView.reloadData()
                }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostViewCell
        if let posts = posts {
            cell.fillCell(post: posts[indexPath.row])
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76.0
    }
    
    @objc func clickOnButton(button: UIButton) {
        performSegue(withIdentifier: "ShowTags", sender: button)
        
    }
    
    @objc func refresh(sender:AnyObject) {
        api.getPosts(category: tag?.slug ?? "tech") {[weak self] response, error in
            self?.posts = response
            self?.refreshControl!.endRefreshing()
            self?.tableView.reloadData()
        }
    }
 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowPost" {
            if let postVC = segue.destination as? PostViewController,
                let cell = sender as? PostViewCell,
                let indexPath = tableView.indexPath(for: cell)
            {
                if let posts = posts {
                    postVC.post = posts[indexPath.row]
                }
            }
        } else if segue.identifier == "ShowTags" {
            if let tagsVC = segue.destination as? TagsViewController {
                if let tags = tags {
                    tagsVC.tags = tags
                }
            }
        }
    }
    

}
