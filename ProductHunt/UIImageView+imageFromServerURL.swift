//
//  UIImageView+imageFromServerURL.swift
//  ProductHunt
//
//  Created by MacBookPro on 12/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//
import UIKit

extension UIImageView {
    public func imageFromServerURL(urlString: String) {
        
        URLSession.shared.dataTask(with: URL(string: urlString)!, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error)
                return
            }
            
            DispatchQueue.main.async {
                let image = UIImage(data: data!)
                self.image = image
            }
            
        }).resume()
    }}
