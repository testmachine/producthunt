//
//  Post.swift
//  ProductHunt
//
//  Created by MacBookPro on 12/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class Post {
    let name: String
    let tagline: String
    let votesCount: Int
    let thumbnailURL: String
    let screenshotURL: String
    let appStoreURL: String
    
    init?(json: [String: AnyObject]) {
        guard let name = json["name"] as? String,
        let tagline = json["tagline"] as? String,
        let votesCount = json["votes_count"] as? Int,
        let thumbnail = json["thumbnail"] as? [String:AnyObject],
        let thumbnailURL = thumbnail["image_url"] as? String,
        let screenshots = json["screenshot_url"] as? [String:AnyObject],
        let screenshotURL = screenshots["850px"] as? String,
        let appStoreURL = json["redirect_url"] as? String
        
        else {
                return nil
    }
        self.name = name
        self.tagline = tagline
        self.votesCount = votesCount
        self.thumbnailURL = thumbnailURL
        self.screenshotURL = screenshotURL
        self.appStoreURL = appStoreURL
    }
    
}
