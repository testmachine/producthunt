//
//  Tag.swift
//  ProductHunt
//
//  Created by MacBookPro on 13/12/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class Tag {
    let id: Int
    let name: String
    let slug: String
    
    init?(json: [String: AnyObject]) {
        guard let id = json["id"] as? Int,
        let name = json["name"] as? String,
        let slug = json["slug"] as? String
            else {
                return nil
        }
        self.id = id
        self.name = name
        self.slug = slug
    }
    
}
